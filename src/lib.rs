use clearscreen::ClearScreen;
use std::collections::HashMap;
use std::io::{self, Write};
use std::{thread, time};

pub const WIDTH: i32 = 75;
pub const HEIGHT: i32 = 20;

pub fn banner(msg: &str) {
    println!(
        "----------------------
{msg}
By Al Sweigart al@inventwithpython.com
(rust-ed 🦀 by lij)
----------------------\n"
    );
}

pub fn get_input(message: &str) -> String {
    println!("{message}");
    print!("> ");
    io::stdout().flush().unwrap();
    let mut guess = String::new();
    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");
    guess.trim().to_string()
}

pub fn instructions() {
    println!(
        "The classic cellular automata simulation.
• Living cells with two or three neighbors stay alive in the next step of the simulation.
• Dead cells with exactly three neighbors become alive in the next step of the simulation.
• Any other cell dies or stays dead in the next step of the simulation
    "
    );
    let mut tmp = String::from("");
    for _ in 0..WIDTH {
        tmp += "=";
    }
    println!("{}", tmp);
}

pub fn simulate() {
    let mut current_board = mutate(&HashMap::new(), true);
    for _idx in 0..300 {
        ClearScreen::default()
            .clear()
            .expect("failed to clear the screen");
        banner("🗿 Conway's Game of Life 🗿");
        instructions();
        print_board(&current_board);
        let new_board = mutate(&current_board, false);
        if new_board == current_board {
            break;
        } else {
            current_board = new_board;
        }
        thread::sleep(time::Duration::from_millis(150));
    }
}

pub fn mutate(board: &HashMap<(i32, i32), bool>, new: bool) -> HashMap<(i32, i32), bool> {
    let mut new_board = HashMap::new();
    for row in 0..HEIGHT {
        for col in 0..WIDTH {
            if new {
                let y = rand::random::<f64>();
                let alive: bool = y > 0.70;
                new_board.insert((row, col), alive);
            } else {
                let cell = board.get(&(row, col)).unwrap();
                let n = get_neighboors((row, col), board);
                new_board.insert((row, col), cell_live(*cell, n));
            }
        }
    }
    new_board
}

pub fn get_neighboors(pair: (i32, i32), board: &HashMap<(i32, i32), bool>) -> i32 {
    let (row, col) = pair;
    let mut counter = 0;
    for idx in -1..2 {
        for jdx in -1..2 {
            let tmp = (row + idx, col + jdx);
            if tmp != pair {
                let neighboor = board.get(&(tmp.0, tmp.1)).unwrap_or(&false);
                if *neighboor {
                    counter += 1;
                }
            }
        }
    }
    counter
}

pub fn cell_live(is_alive: bool, neighboors: i32) -> bool {
    let mut new_status = false;
    if (is_alive && (2..=3).contains(&neighboors)) || neighboors == 3 {
        new_status = true;
    }
    new_status
}

pub fn print_board(board: &HashMap<(i32, i32), bool>) {
    for row in 0..HEIGHT {
        let mut tmp = String::new();
        for col in 0..WIDTH {
            let cell = board.get(&(row, col)).unwrap();
            if *cell {
                tmp.push('🦀')
            } else {
                tmp.push(' ')
            }
        }
        println!("{}", tmp);
    }
    let mut tmp = String::new();
    for _ in 0..WIDTH {
        tmp += "=";
    }
    println!("{}", tmp);
    println!("... simulating life ...")
}
