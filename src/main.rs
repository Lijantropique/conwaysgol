use clearscreen::ClearScreen;
fn main() {
    ClearScreen::default()
        .clear()
        .expect("failed to clear the screen");
    conwaysgol::simulate();
}
